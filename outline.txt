Charlie reston
Can we tell the story of a man's life by the changing things he cries about?

setting: home
Angela is a new mother trying to build her family and wants to create the best christmas ever
But can her Angela make Christmas happy if she can't figure out how to make her baby happy?
Angela is the protagonist.
She tries to make a perfect christmas, but has taken on too much.
her challenges: decorate the house, cook, make charlie happy
how to make charlie happy: 
* pick him up
* sing him a song
** Rubber tree plant
** aint got a barrel of money
* feed him

Something burns in the kitchen?
Problem with Bob?
Wanting and reaching out for something, but can't figure it out.... 
as a baby at christmas
as a baby at his brother's birthday
wants a cake
but cannot have it.
he cries and cries
should it be from his viewpoint or the third person?


Setting: school
Charlie is in a new school and must try to fit in and make friends.
But can Charlie be cool when he gets picked last for dodgeball by the cool kids on the playground?
as a boy in school
waiting on the playground 
to be picked
he isn't
and he cries that he isn't part of the group

as a young man in high school
transition to college
his first love
dating this perfect girl
she has a cool coat, great hair, smells wonderful
she's the best
but she's going to berkley
and her family is moving back west
he's going to the local community college
he can't possibly make the trip
they won't even see each other on holidays
he weeps that life is so unfair to him

as an older man
he got a job
he's living in boston
his parents moved down to florida
his mother is calling him
"You know I think you need to come down to see your dad."
"I've been in the hospital the last few days with your dad.
he's not doing so great.
Dr. Rosenthal says he may pull through
but he's not responding to the chemo so well."

Off the phone he talks to his wife
and he wonders how his mom will cope
he doesn't know the state of their finances
We may need to help them.
And he cries for his father's pain.
such a great man and for it to all burn away like this.


And the waters of the flood close over New Orleans
and he's watching the news 
and looking at people hacking there way out of there attics
with axes
and he knows that some of them don't have axes
and will die in the attic
and he cries for them.


